﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameModule : MonoBehaviour
{


	///<summary>A list of places in the game.</summary>
	public List<Place> places = new List<Place>();
	public List<Monster> monsterList = new List<Monster>();
	public List<Loot> lootTableList = new List<Loot>();
	
	public void AddModuleToGame()
	{
        
		// add places
		foreach (Place placeToAdd in places)
		{
			bool duplicate = false;
			// check if we already have that place!
			foreach (Place placeToCheckForDuplication in Game.Instance.places)
			{
				if (placeToAdd.name == placeToCheckForDuplication.name)
				{
					duplicate=true;
					Debug.LogWarning("DUPLICATE PLACE NAME IN MODULE!");
				}
			}
			// if no duplicates, add it
			if (!duplicate)
			{
				Game.Instance.places.Add(placeToAdd);
			}
		}
		// add monsters
		foreach (Monster mToAdd in monsterList)
		{
			bool duplicate = false;
			// check if we already have that!
			foreach (Monster mToCheckForDuplication in Game.Instance.monsterList)
			{
				if (mToAdd.name == mToCheckForDuplication.name)
				{
					duplicate=true;
					Debug.LogWarning("DUPLICATE MONSTER NAME IN MODULE!");
				}
			}
			// if no duplicates, add it
			if (!duplicate)
			{
				Game.Instance.monsterList.Add(mToAdd);
			}
		}
		// add Loots
		foreach (Loot lToAdd in lootTableList)
		{
			bool duplicate = false;
			// check if we already have that place!
			foreach (Loot lToCheckForDuplication in Game.Instance.lootTableList)
			{
				if (lToAdd.name == lToCheckForDuplication.name)
				{
					duplicate=true;
					Debug.LogWarning("DUPLICATE PLACE NAME IN MODULE!");
				}
			}
			// if no duplicates, add it
			if (!duplicate)
			{
				Game.Instance.lootTableList.Add(lToAdd);
			}
		}
		
	}
	
	// Use this for initialization
	void Start () {
		AddModuleToGame();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
