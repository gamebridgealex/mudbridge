﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
/// A place defines a location in the world, its exits, its spawn probabilities for monsters and loots.
public class Place
{
	public string name = "Test Planet";
	public string description = "Test planet, please ignore. To the south is the south pole. To the north is the north pole. To the east leads back to here. To the west leads back to here.";
	public List<Exit> exits = new List<Exit>();
    public string type = "";

	public List<string> monsterNames = new List<string>();
	public int monsterChance = 50;
	public List<string> lootNames = new List<string>();
	public int lootChance = 50;
	public delegate void MultiDelegate();
	public MultiDelegate onPlayerEnter;
}