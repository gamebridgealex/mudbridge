﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Mudbridge {
	public class MonitorControlMode : MonoBehaviour {
		
		[System.Serializable]
		public class ControlMode {
			public string name = "";
			public List<GameObject> objectsToEnable;
			public List<GameObject> objectsToDisable;
			public KeyCode keyToEscape;
			public bool animateCamera = false;
		}
		
		public List<ControlMode> controlModes;
		public ControlMode currentMode;
		public int currentModeIndex = 0;
		
		public bool debugNext = false;
		
		public Camera cameraToAnimate;
		public Camera cameraToAnimateTweenTarget;
		
		public float animateStartTime = 0f;
		public bool doingCameraAnimation = false;
		public Vector3 originalCameraPosition = Vector3.zero;
		public Transform targetCameraTransform;
		public float animateSpeedFactor = 1f;
		#region methods
		
		
		public void StartAnimatingCamera()
		{
			animateStartTime = Time.time;	
			doingCameraAnimation = true;
			originalCameraPosition = Camera.main.transform.position;
		}
		
		public void AnimateCamera()
		{
			if ( doingCameraAnimation)
			{
				float amount = Time.time - animateStartTime * animateSpeedFactor;
				
				Camera.main.transform.position = Vector3.Lerp(originalCameraPosition, targetCameraTransform.position, amount);
				if (amount>1f)
				{
					doingCameraAnimation = false;
				}
			}
		}
		
		public void SetMode(int modeIndex) 
		{
			currentModeIndex = modeIndex;
			currentMode = controlModes[modeIndex];
			foreach (GameObject go in currentMode.objectsToEnable)
			{
				go.SetActive(true);
			}
			foreach (GameObject go in currentMode.objectsToDisable)
			{
				go.SetActive(false);
			}
			if (currentMode.animateCamera){ 
				StartAnimatingCamera(); 
			}
			
		}
		
		public void NextMode()
		{
			if (controlModes.Count > 0)
			{
				// count is 1 higher than index of last item
				// Are we at the last mode already?
				if ( currentModeIndex == controlModes.Count - 1 )
				{
					SetMode(0);
				}
				else
				{
					SetMode(currentModeIndex+1);
				}
			}
		}
		
		
		#endregion methods
		
		#region Unity methods
	
		// Use this for initialization
		void Start () {
			SetMode(currentModeIndex);
		}
		
		// Update is called once per frame
		void Update () {
			if(debugNext) { NextMode(); debugNext = false; }
			if (Input.GetKeyDown(currentMode.keyToEscape))
			{
				NextMode();
			}
		}
		#endregion
	}
}
