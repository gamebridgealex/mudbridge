﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShoppingMall : MonoBehaviour {
	
	bool waitingForInput = false;
	int numberOfItemsInStore = 0;
	
	//List of Items ID in store avalible for purchase
	List<string> ItemsInStore = new List<string>();
	
	//List for all items out of stock
	string outOfStockItems = "";
	
	//Boolean to say we are currently viewing the contents of target.
	bool browsing = true;
	
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		

		//Check if we are at target
		if (Game.Instance.currentPlaceName == "Target")
		{
			//if we are not browsing target
			if (!browsing)
			{
				//Start browsing target
				if(Input.GetKeyDown(KeyCode.B))
				{
					browsing = true;
				}
			}
			
			//Check if we are browsing
			if (browsing)
			{
				//Check text is printed and only printed once
				if (Game.Instance.printDelayedBufferString.Length<=0 & waitingForInput == false)
				{
					//Tell the player how much money they have
					Game.Print("You have " + Game.Instance.currency + " " + Game.Instance.currencyName);
					//Display a list of items to buy
					Game.Print("Below is a list of items you can buy, press the number for the item you want to buy.");
					
					int itemID = 1; //number to press to buy this item
					
					//Add loot to stocklist and displaying loot
					
					//Go through all loot
					foreach(Loot currentItem in Game.Instance.lootTableList)
					{
						//Check if price is not -1
						if (currentItem.costValue != -1)
						{
							if (Random.value > 0.1) //With a 1/10 chance of not dispaying that item
							{
								//Display loot name and cost. eg "1 - Laser Pistol : $100"
								Game.Print(itemID++ + " - " + currentItem.displayName + " : $" + currentItem.costValue);
								
								//Save item to list of items in store
								ItemsInStore.Add(currentItem.name);
							}
							else
							//Add item t out of stock list
							{
								outOfStockItems = outOfStockItems + ", " + currentItem.displayName;
							}
						}
				
					}
					if (outOfStockItems != "")
						Game.Print("We are out of stock for " + outOfStockItems);
					
					//Display the exit for the store
					Game.Print("Press 'Esc' to leave store");
					
					//Wait for input from player
					waitingForInput = true;
					
					//Save number of items in the store
					numberOfItemsInStore = itemID;
				}
				
				//Check if we are triyig to leave the store by pressing 'Esc'
				if (Input.GetKeyDown(KeyCode.Escape)) //This checks if Esc ahs been pressed
				{
					browsing = false;//turn off browsing store mode
					
					//Display how to leave the store 
					Game.Instance.PlaceMenuPrint();
					
					//Display how to get back into the store
					Game.Print("Press 'B' to start browsing again.");
				}else
				{
				
					//Loop through all items to check if each has been pressed
					for (int i = 0; i <= numberOfItemsInStore; i++)
					{
						//Check for input of the number of the item
						if (Input.GetKeyDown(i.ToString()))
						{
							
							int price = 0;
							
							//Find the correct item
							foreach(Loot item in Game.Instance.lootTableList)
							{
								//Found it! By checking the name
								if (item.name == ItemsInStore[i-1])
								{
									//Save the price
									price = item.costValue;
									
									//Check player can afford item
									if (Game.Instance.currency >= price)
									{
									
										//Add item to inventory
										Game.Instance.lootInventoryList.Add(item);
										
										//Tell the player the they just did
										Game.Print("You just bought a " + item.displayName + " for " + item.costValue);
										//Subtrat cost from curreny
										Game.Instance.currency -= price;
										//Tell the player how much money they ave left
										Game.Print("You have " + Game.Instance.currency + " " + Game.Instance.currencyName + " left");		
									}
									else //you can not afford the item
									{
										Game.Print("You are too poor for that");
									}
									
									//exit loop
									break;
								}
							}
						}
					}
				
					//Check for any button to redraw the shop
					if (Input.anyKeyDown)
					{
						waitingForInput = false;
					}
				}
			}
		}
		
		
		
		
	}
}
